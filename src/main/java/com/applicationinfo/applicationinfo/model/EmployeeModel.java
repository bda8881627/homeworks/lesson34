package com.applicationinfo.applicationinfo.model;

import lombok.Data;


@Data
public class EmployeeModel {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
}
