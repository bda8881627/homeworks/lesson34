package com.applicationinfo.applicationinfo.service;

import com.applicationinfo.applicationinfo.model.EmployeeModel;

import java.util.List;

public interface EmployeeService {
    List<EmployeeModel> getAllEmployees();
    EmployeeModel getEmployeeById(Long id);
    void addEmployee(EmployeeModel employee);
    void updateEmployee(Long id, EmployeeModel employee);
    void deleteEmployee(Long id);
}
