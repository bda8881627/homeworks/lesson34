package com.applicationinfo.applicationinfo.service;

import com.applicationinfo.applicationinfo.model.EmployeeModel;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class EmployeeServiceImpl implements EmployeeService{
    @Override
    public List<EmployeeModel> getAllEmployees() {
        return null;
    }

    @Override
    public EmployeeModel getEmployeeById(Long id) {
        return null;
    }

    @Override
    public void addEmployee(EmployeeModel employee) {

    }

    @Override
    public void updateEmployee(Long id, EmployeeModel employee) {

    }

    @Override
    public void deleteEmployee(Long id) {

    }
}
