package com.applicationinfo.applicationinfo.controller;

import com.applicationinfo.applicationinfo.model.EmployeeModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private List<EmployeeModel> employees = new ArrayList<>();

    @GetMapping
    public List<EmployeeModel> getAllEmployees() {
        return employees;
    }

    @GetMapping("/{id}/{name}")
    public EmployeeModel getEmployeeById(@PathVariable Long id,@PathVariable String name) {
        return null;
    }

}
