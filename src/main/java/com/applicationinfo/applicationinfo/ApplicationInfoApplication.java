package com.applicationinfo.applicationinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ApplicationInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationInfoApplication.class, args);
	}

}
